import os, sys, argparse, binascii


os.system("mips-linux-gnu-as -mips32 inst_rom.S -o rom.o")
os.system("mips-linux-gnu-ld -T ram.ld rom.o -o rom.om")
os.system("mips-linux-gnu-objcopy -O binary rom.om rom.bin")

with open("rom.txt", "w") as f:
        s = binascii.b2a_hex(open("rom.bin", "rb").read())
        for i in xrange(len(s) / 8):
                print >> f, s[i * 8 : (i + 1) * 8]

os.system("rm rom.o rom.om rom.bin")
