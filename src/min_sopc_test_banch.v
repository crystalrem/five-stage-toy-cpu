`include "define.v"
`include "mips_min_sopc.v"

module min_sopc_test_banch();

	reg	CLOCK_50;
	reg	rst;

	initial begin
		CLOCK_50 = 1'b0;
		forever #10 CLOCK_50 = ~CLOCK_50;
	end

	initial begin
		rst = `RstEnable;
		#195 rst = `RstDisable;
		#2000 $stop;
	end
	
	mips_min_sopc mips_min_sopc0(
		.clk(CLOCK_50),
		.rst(rst)
	);

endmodule
