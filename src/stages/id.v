`include "define.v"
module id(
	input	wire				rst,
	input	wire[`InstAddrBus]	pc_i,
	input	wire[`InstBus]		inst_i,

	input wire[`AluOpBus]		ex_aluop_i,
//*** Read From Regfile ***
	input	wire[`RegBus]		reg1_data_i,
	input	wire[`RegBus]		reg2_data_i,

// + *** Information From Ex Process *** + 
	input	wire				ex_wreg_i,
	input	wire[`RegBus]		ex_wdata_i,
	input	wire[`RegAddrBus]	ex_wd_i,

// + *** Information From Mem Process *** +
	input	wire				mem_wreg_i,
	input	wire[`RegBus]		mem_wdata_i,
	input	wire[`RegAddrBus]	mem_wd_i,	

	input	wire[5:0]			stall, 
	
	input	wire				is_in_delayslot_i,

//*** Write To Regfile ***
	output	reg					reg1_read_o,
	output	reg					reg2_read_o,
	output	reg[`RegAddrBus]	reg1_addr_o,
	output	reg[`RegAddrBus]	reg2_addr_o,
//*** Send To Process ***
	output	reg[`AluOpBus]		aluop_o,
	output	reg[`AluSelBus]		alusel_o,
	output	reg[`RegBus]		reg1_o,
	output	reg[`RegBus]		reg2_o,
	output	reg[`RegAddrBus]	wd_o,
	output	reg					wreg_o,
	output  wire[`RegBus]       inst_o,

	output	wire                stallreq,	
	
	output	reg					next_inst_in_delayslot_o,
	
	output	reg					branch_flag_o,
	output	reg[`RegBus]		branch_target_address_o,
	output	reg[`RegBus]		link_addr_o,
	output	reg					is_in_delayslot_o
);

	wire[5:0] op = inst_i[31:26];
	wire[4:0] op2 = inst_i[10:6]; //sa := op2
	wire[5:0] op3 = inst_i[5:0]; //func := op3
	wire[4:0] op4 = inst_i[20:16];  //rt := op4
	wire[`RegBus] pc_plus_8;
	wire[`RegBus] pc_plus_4;
	wire[`RegBus] imm_sll2_signedext;  
//*** imm ***
	reg[`RegBus]	imm;

//*** Instruction Valid ***
	reg instvalid;

	reg stallreq_for_reg1_loadrelate;
	reg stallreq_for_reg2_loadrelate;
	wire pre_inst_is_load;

	assign inst_o = inst_i;
	assign pc_plus_8 = pc_i + 8;
	assign pc_plus_4 = pc_i + 4;
	assign imm_sll2_signedext = {{14{inst_i[15]}}, inst_i[15:0], 2'b00 };  
	assign stallreq = stallreq_for_reg1_loadrelate | stallreq_for_reg2_loadrelate;
	assign pre_inst_is_load = ((ex_aluop_i == `EXE_LB_OP) || (ex_aluop_i == `EXE_LW_OP)) ? 1'b1 : 1'b0;

	
	
	
//*** Instruction Translation ***

	always @ (*) begin
		if(rst == `RstEnable) begin
			aluop_o		<= `EXE_NOP_OP; 
			alusel_o	<= `EXE_RES_NOP;
			wd_o		<= `NOPRegAddr;
			wreg_o		<= `WriteDisable;
			instvalid	<= `InstValid; //?
			reg1_read_o	<= `ReadDisable; //ReadEnable?
			reg2_read_o	<= `ReadDisable;
			reg1_addr_o	<= `NOPRegAddr;
			reg2_addr_o	<= `NOPRegAddr;
			imm			<= 32'h0;
			link_addr_o	<= `ZeroWord;
			branch_target_address_o <= `ZeroWord;
			branch_flag_o			<= `NotBranch;
			next_inst_in_delayslot_o<= `NotInDelaySlot;	
		end else begin
			aluop_o		<= `EXE_NOP_OP;
			alusel_o	<= `EXE_RES_NOP;
			wd_o		<= inst_i[15:11]; //wd_o = rd;
			wreg_o		<= `WriteDisable;
			instvalid	<= `InstInvalid;
			reg1_read_o	<= `ReadDisable;
			reg2_read_o	<= `ReadDisable;
			reg1_addr_o <= inst_i[25:21]; 
			reg2_addr_o	<= inst_i[20:16];
			imm			<= `ZeroWord;		
			link_addr_o	<= `ZeroWord;
			branch_target_address_o <= `ZeroWord;
			branch_flag_o 			<= `NotBranch;
			next_inst_in_delayslot_o<= `NotInDelaySlot;

			/*case(op)
				//*** Instruction Op := ori ***
				`EXE_ORI:	begin
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_OR_OP;
					//***Instuction Type Is Logic Type *** 
					alusel_o	<= `EXE_RES_LOGIC;
					reg1_read_o 	<= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					imm 		<= {16'h0, inst_i[15:0]};
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end		

				default: begin
			
				end
			endcase*/

			case (op)
				`EXE_SPECIAL_INST: begin
					case (op2)
						5'b00000: begin
							case (op3)
								`EXE_OR: begin
									wreg_o		<= `WriteEnable;
									aluop_o		<= `EXE_OR_OP;
									alusel_o	<= `EXE_RES_LOGIC;
									reg1_read_o	<= `ReadEnable;
									reg2_read_o	<= `ReadEnable;
									instvalid	<= `InstValid;
								end
								`EXE_AND: begin
									wreg_o		<= `WriteEnable;
									aluop_o		<= `EXE_AND_OP;
									alusel_o	<= `EXE_RES_LOGIC;
									reg1_read_o	<= `ReadEnable;
									reg2_read_o	<= `ReadEnable;
									instvalid	<= `InstValid;
								end

								`EXE_XOR: begin
									wreg_o		<= `WriteEnable;
									aluop_o		<= `EXE_XOR_OP;
									alusel_o	<= `EXE_RES_LOGIC;
									reg1_read_o	<= `ReadEnable;
									reg2_read_o	<= `ReadEnable;
									instvalid	<= `InstValid;
								end

								`EXE_SLT: begin
									wreg_o		<= `WriteEnable;
									aluop_o		<= `EXE_SLT_OP;
									alusel_o	<= `EXE_RES_ARITHMETIC;
									reg1_read_o	<= `ReadEnable;
									reg2_read_o	<= `ReadEnable;
									instvalid	<= `InstValid;
								end					

								`EXE_ADD: begin
									wreg_o		<= `WriteEnable;
									aluop_o		<= `EXE_ADD_OP;
									alusel_o	<= `EXE_RES_ARITHMETIC;
									reg1_read_o	<= `ReadEnable;
									reg2_read_o	<= `ReadEnable;
									instvalid	<= `InstValid;
								end					

								`EXE_SUB: begin
									wreg_o		<= `WriteEnable;
									aluop_o		<= `EXE_SUB_OP;
									alusel_o	<= `EXE_RES_ARITHMETIC;
									reg1_read_o	<= `ReadEnable;
									reg2_read_o	<= `ReadEnable;
									instvalid	<= `InstValid;
								end	
								
								`EXE_JR: begin
									wreg_o		<= `WriteDisable;
									aluop_o		<= `EXE_JR_OP;
									alusel_o	<= `EXE_RES_JUMP_BRANCH;
									reg1_read_o <= `ReadEnable;
									reg2_read_o	<= `ReadDisable;
									link_addr_o	<= `ZeroWord;
									branch_target_address_o	<= reg1_o;
									branch_flag_o			<= `Branch;
									next_inst_in_delayslot_o<= `InDelaySlot;
									instvalid	<= `InstValid;
								end
												
								default: begin
								
								end
							endcase
						end

						default: begin

						end
					endcase
				end
				
				`EXE_ORI: begin
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_OR_OP;
					alusel_o	<= `EXE_RES_LOGIC;
					reg1_read_o	<= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					imm		<= {16'h0, inst_i[15:0]};
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end
	
				`EXE_ANDI: begin
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_AND_OP;
					alusel_o	<= `EXE_RES_LOGIC;
					reg1_read_o	<= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					imm		<= {16'h0, inst_i[15:0]};
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end

				`EXE_XORI: begin
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_XOR_OP;
					alusel_o	<= `EXE_RES_LOGIC;
					reg1_read_o	<= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					imm		<= {16'h0, inst_i[15:0]};
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end

				`EXE_LUI: begin 
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_OR_OP;
					alusel_o	<= `EXE_RES_LOGIC;
					reg1_read_o	<= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					imm		<= {inst_i[15:0], 16'h0};
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end

				`EXE_SLTI: begin 
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_SLT_OP;
					alusel_o	<= `EXE_RES_ARITHMETIC;
					reg1_read_o	<= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					imm		<= {{16{inst_i[15]}}, inst_i[15:0]};
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end

				`EXE_ADDI: begin 
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_ADDI_OP;
					alusel_o	<= `EXE_RES_ARITHMETIC;
					reg1_read_o	<= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					imm		<= {{16{inst_i[15]}}, inst_i[15:0]};
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end
				
				`EXE_J: begin
					wreg_o		<= `WriteDisable;
					aluop_o		<= `EXE_J_OP;
					alusel_o	<= `EXE_RES_JUMP_BRANCH;
					reg1_read_o <= `ReadDisable;
					reg2_read_o	<= `ReadDisable;
					link_addr_o	<= `ZeroWord;
					branch_flag_o			<= `Branch;
					next_inst_in_delayslot_o<= `InDelaySlot;
					instvalid	<= `InstValid;
					branch_target_address_o	<= {pc_plus_4[31:28], inst_i[25:0], 2'b00};
				end
				
				`EXE_BEQ: begin
					wreg_o		<= `WriteDisable;
					aluop_o		<= `EXE_BEQ_OP;
					alusel_o	<= `EXE_RES_JUMP_BRANCH;
					reg1_read_o <= `ReadEnable;
					reg2_read_o	<= `ReadEnable;
					link_addr_o	<= `ZeroWord;
					instvalid	<= `InstValid;
					if(reg1_o == reg2_o) begin
						branch_target_address_o	<= pc_plus_4 + imm_sll2_signedext;
						branch_flag_o			<= `Branch;
						next_inst_in_delayslot_o<= `InDelaySlot;
					end
				end
								
				`EXE_BNE: begin
					wreg_o		<= `WriteDisable;
					aluop_o		<= `EXE_BLEZ_OP;
					alusel_o	<= `EXE_RES_JUMP_BRANCH;
					reg1_read_o <= `ReadEnable;
					reg2_read_o	<= `ReadEnable;
					link_addr_o	<= `ZeroWord;
					instvalid	<= `InstValid;
					if(reg1_o != reg2_o) begin
						branch_target_address_o	<= pc_plus_4 + imm_sll2_signedext;
						branch_flag_o			<= `Branch;
						next_inst_in_delayslot_o<= `InDelaySlot;
					end
				end
				
				`EXE_LB: begin
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_LB_OP;
					alusel_o	<= `EXE_RES_LOAD_STORE;
					reg1_read_o <= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end
				
				`EXE_LW: begin
					wreg_o		<= `WriteEnable;
					aluop_o		<= `EXE_LW_OP;
					alusel_o	<= `EXE_RES_LOAD_STORE;
					reg1_read_o <= `ReadEnable;
					reg2_read_o	<= `ReadDisable;
					wd_o		<= inst_i[20:16];
					instvalid	<= `InstValid;
				end
				
				`EXE_SB: begin
					wreg_o		<= `WriteDisable;
					aluop_o		<= `EXE_SB_OP;
					reg1_read_o <= `ReadEnable;
					reg2_read_o	<= `ReadEnable;
					instvalid	<= `InstValid;
					alusel_o	<= `EXE_RES_LOAD_STORE;
				end
				
				`EXE_SW: begin
					wreg_o		<= `WriteDisable;
					aluop_o		<= `EXE_SW_OP;
					reg1_read_o <= `ReadEnable;
					reg2_read_o	<= `ReadEnable;
					instvalid	<= `InstValid;
					alusel_o	<= `EXE_RES_LOAD_STORE;
				end
				
				default: begin
		
				end
			endcase
		end //end else
	end //end always

	//*** Get Reg1_o ***
	/*always @ (*) begin
		if(rst == `RstEnable) begin
			reg1_o <= `ZeroWord;
		end else if(reg1_read_o == `ReadEnable) begin
			reg1_o <= reg1_data_i;
		end else if(reg1_read_o == `ReadDisable) begin
			reg1_o <= imm; //?
		end else begin
			reg1_o <= `ZeroWord;
		end
	end*/

	// + *** Get Reg1_o *** + 
	/*always @ (*) begin
		if(rst == `RstEnable) begin
			reg1_o <= `ZeroWord;
		end else if((reg1_read_o == `ReadEnable) && (ex_wreg_i == 1'b1) && (ex_wd_i == reg1_addr_o)) begin
			reg1_o <= ex_wdata_i;
		end else if((reg1_read_o == `ReadEnable) && (mem_wreg_i == 1'b1) && (mem_wd_i == reg1_addr_o)) begin
			reg1_o <= mem_wdata_i;
		end else if(reg1_read_o == `ReadEnable) begin
			reg1_o <= reg1_data_i;
		end else if(reg1_read_o == `ReadDisable) begin
			reg1_o <= imm; //?
		end else begin
			reg1_o <= `ZeroWord;
		end
	end*/
	
	always @ (*) begin
		stallreq_for_reg1_loadrelate <= `NoStop;	
		if(rst == `RstEnable) begin
			reg1_o <= `ZeroWord;	
		end else if(pre_inst_is_load == 1'b1 && ex_wd_i == reg1_addr_o 
								&& reg1_read_o == 1'b1 ) begin
		  stallreq_for_reg1_loadrelate <= `Stop;							
		end else if((reg1_read_o == 1'b1) && (ex_wreg_i == 1'b1) 
								&& (ex_wd_i == reg1_addr_o)) begin
			reg1_o <= ex_wdata_i; 
		end else if((reg1_read_o == 1'b1) && (mem_wreg_i == 1'b1) 
								&& (mem_wd_i == reg1_addr_o)) begin
			reg1_o <= mem_wdata_i; 			
		end else if(reg1_read_o == 1'b1) begin
			reg1_o <= reg1_data_i;
		end else if(reg1_read_o == 1'b0) begin
			reg1_o <= imm;
		end else begin
			reg1_o <= `ZeroWord;
		end
		//#100 $display("id-reg1_o = %b",reg1_o);
	end

	//*** Get Reg2_o ***
	/*always @ (*) begin
		if(rst == `RstEnable) begin
			reg2_o <= `ZeroWord;
		end else if(reg2_read_o == `ReadEnable) begin
			reg2_o <= reg2_data_i;
		end else if(reg2_read_o == `ReadDisable) begin
			reg2_o <= imm; 
		end else begin
			reg2_o <= `ZeroWord;
		end
	end*/

	// + *** Get Reg2_o *** + 
	/*always @ (*) begin
		if(rst == `RstEnable) begin
			reg2_o <= `ZeroWord;
		end else if((reg2_read_o == `ReadEnable) && (ex_wreg_i == 1'b1) && (ex_wd_i == reg2_addr_o)) begin
			reg2_o <= ex_wdata_i;
		end else if((reg2_read_o == `ReadEnable) && (mem_wreg_i == 1'b1) && (mem_wd_i == reg2_addr_o)) begin
			reg2_o <= mem_wdata_i;
		end else if(reg2_read_o == `ReadEnable) begin
			reg2_o <= reg2_data_i;
		end else if(reg2_read_o == `ReadDisable) begin
			reg2_o <= imm; //?
		end else begin
			reg2_o <= `ZeroWord;
		end
	end*/
	always @ (*) begin
		stallreq_for_reg2_loadrelate <= `NoStop;
		if(rst == `RstEnable) begin
			reg2_o <= `ZeroWord;
		end else if(pre_inst_is_load == 1'b1 && ex_wd_i == reg2_addr_o 
							&& reg2_read_o == 1'b1 ) begin
			stallreq_for_reg2_loadrelate <= `Stop;			
		end else if((reg2_read_o == 1'b1) && (ex_wreg_i == 1'b1) 
							&& (ex_wd_i == reg2_addr_o)) begin
			reg2_o <= ex_wdata_i; 
		end else if((reg2_read_o == 1'b1) && (mem_wreg_i == 1'b1) 
								&& (mem_wd_i == reg2_addr_o)) begin
			reg2_o <= mem_wdata_i;			
		end else if(reg2_read_o == 1'b1) begin
			reg2_o <= reg2_data_i;
		end else if(reg2_read_o == 1'b0) begin
			reg2_o <= imm;
		end else begin
			reg2_o <= `ZeroWord;
		end
		//#100 $display("id-reg2_o = %b",reg1_o);
	end
	
	always @ (*) begin
		if(rst == `RstEnable) begin
			is_in_delayslot_o <= `NotInDelaySlot;
		end else begin
			is_in_delayslot_o <= is_in_delayslot_i;
		end
	end

endmodule

	

					
